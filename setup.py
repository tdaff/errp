"""
Install
"""

from setuptools import setup, find_packages

setup(name='errp',
      version='1.0.0',
      description='Election Results Relay Parser',
      long_description=open('README.rst').read(),
      author='Tom Daff',
      author_email='tdd20@cam.ac.uk',
      license='BSD',
      url='https://github.com/tdaff/errp/',
      packages=find_packages(exclude=['tests']),
      entry_points={
          'console_scripts':
              ['errpd = errp.webapp.__main__:main']},
      package_data={
          'errp': ['webapp/templates/*.html']},
      install_requires=['flask'],
      extras_require={'test': ['pytest']},
      classifiers=[
          'Programming Language :: Python :: 3',
          'License :: OSI Approved :: BSD License'])


