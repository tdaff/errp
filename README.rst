ERRP
====

Election Results Relay Parser. Parse a feed of election results
and transform into a standardised format that you can work with!

``errp`` will take specific short format election results and
parse them into formats more easily read by humans and machines.
Short party names are converted to full names and the results of
each constituency are analysed and collected.

Relevant links:

- https://bitbucket.org/tdaff/errp
- https://hub.docker.com/r/tdd20/errp/

Quickstart
----------

Docker is a great way to get up and running:

.. code:: shell

    docker run -p 5000:5000 tdd20/errp

Browse to ``http://<your_docker_ip>:5000/`` to view recent results
in a web page. A description of API access is given below.

Alternatively you can clone the source code, use ``pip install``
to ensure that it is in your ``$PATH`` and run the command
``errpd`` to start the server.

Requirements for running the code are:

- Python >= 3.5
- Flask

Options
-------

By default the code uses dummy data, to specify the URL of real
data use the ``-s`` argument.

.. code:: shell

    # for Docker

    docker run -p 5000:5000 tdd20/errp errpd -s http://url_of_file.com/res.dat

    # for commandline

    errpd -s http://url_of_file.com/res.dat

API
---

- **Party Data**
    - Number of seats held by each party.
    - ``/api/v1.0/parties/``
    - ``GET``
    - Example ::

        {"Conservative Party": 0,
         "Green Party": 0,
         "Independent": 0,
         "Labour Party": 3,
         "Liberal Democrats": 0,
         "SNP": 0,
         "UKIP": 0,
         "timestamp": "Mon, 28 Aug 2017 22:58:02 GMT"}


- **Constituency Data**
    - Breakdown of votes for each constituency. Includes percentages.
    - ``/api/v1.0/constituencies/``
    - ``GET``
    - Example ::

        {"Elsewhere Shire, North": {
            "vote_percent": {
                "Conservative Party": 27.455357142857142,
                "Labour Party": 67.63392857142857,
                "Liberal Democrats": 4.910714285714286
            },
            "votes": {
                "Conservative Party": 123,
                "Labour Party": 303,
                "Liberal Democrats": 22
            },
            "winner": "Labour Party"
        }, ... }


- **Parsing Errors**
    - List of errors encountered during parsing.
    - ``/api/v1.0/errors``
    - ``GET``
    - Example ::

        {"0": "Malformed data on line 3: Here is a, bad, result that , doesn't work, bad",
         "1": "No data found on line 4: Noresults Town",
         "timestamp": "Mon, 28 Aug 2017 23:05:18 GMT"}


- **All Data**
    - ``/api/v1.0/``
    - ``GET``
    - Content ::

        {'parties': <party_data>,
         'constituencies': <constituency_data>,
         'errors': <error_data>,
         'timestamp': <timestamp_of_data>}
