"""
Flask app as the frontend.

"""

import argparse
import urllib.request
from datetime import datetime

from flask import Flask, render_template, jsonify


from errp.parser import parse_data

# Flask
app = Flask(__name__)


DUMMY_DATA = """Cardiff West, 11014, C, 17803, L, 4923, UKIP, 2069, LD
Islington South & Finsbury, 22547, L, 9389, C, 4829, LD, 3375, UKIP, 3371, G, 309, Ind
Elsewhere Shire, North, 123, C, 303, L, 22, LD
Here is a, bad, result that , doesn't work, bad
Noresults Town
"""


def get_data():
    """Retrieve the raw data from source defined in config, or dummy.

    Return the data as a string."""
    data_source = app.config.get('source')
    if data_source == 'dummy':
        return DUMMY_DATA
    else:
        resource = urllib.request.urlopen(data_source)
        return resource.read().decode(resource.headers.get_content_charset())


# Index page
@app.route('/')
def view_results():
    """Render the results server as the root page."""
    constituencies, parties, errors = parse_data(get_data())
    return render_template('results.html', constituencies=constituencies,
                           parties=parties, errors=errors)


# API implemented here!
@app.route('/api/v1.0/', methods=['GET'])
@app.route('/api/v1.0/<string:part>', methods=['GET'])
# Current version of the API
@app.route('/api/', methods=['GET'])
@app.route('/api/<string:part>', methods=['GET'])
def api_data_1_0(part=''):
    """Return the parsed data, or a subset thereof. Response is a json
    string with a timestamp.

    Parameters
    ----------
    part: str
        Any of 'constituencies', 'parties', 'errors'. The requested
        data will be the root of the returned json. If nothing is
        requested or an unrecognised string is requested, return all
        parts in the json object."""

    constituencies, parties, errors = parse_data(get_data())
    if part.lower() == 'constituencies':
        requested = constituencies
    elif part.lower() == 'parties':
        requested = parties
    elif part.lower() == 'errors':
        requested = {'{}'.format(idx): error
                     for idx, error in enumerate(errors)}
    else:
        requested = {
            'constituencies': constituencies,
            'parties': parties,
            'errors': errors}
    # add a timestamp
    requested['timestamp'] = datetime.now()

    return jsonify(requested)


# Have main handle any extra arguments needed to customise
# the application at runtime
def main():
    parser = argparse.ArgumentParser(description='Election results '
                                                 'relay parser')
    parser.add_argument('-s', '--source', default='dummy',
                        help='Resource to obtain raw results from. '
                             'Defaults to mock data.')
    args = parser.parse_args()

    app.config['source'] = args.source
    app.run(host='0.0.0.0')


if __name__ == '__main__':
    main()
