"""
parser.py

Functions that convert text based source data into a more manageable
structured format.

"""

from collections import OrderedDict

from errp.log import error

CODES_TO_NAMES = {
    'C': 'Conservative Party',
    'L': 'Labour Party',
    'UKIP': 'UKIP',
    'LD': 'Liberal Democrats',
    'G': 'Green Party',
    'Ind': 'Independent',
    'SNP': 'SNP'}

NAMES_TO_CODES = {name: code for code, name in CODES_TO_NAMES.items()}


def to_long_name(code):
    """Convert a party code to a party name.

    Conversion is case insensitive and will also accept the long name
    which will be returned in title case.

    Parameters
    ----------
    code: str
        Short code for the party.

    Returns
    -------
    name: str
        Long name of the party.

    Raises
    ------
    KeyError:
        Value passes that is not recognised as a party.
    ValueError:
        Non-string value passed.

    """

    if not hasattr(code, 'lower'):
        raise ValueError('Cannot interpret {} as a string'.format(code))

    # Straight case insensitive conversion
    for code_cmp, name in CODES_TO_NAMES.items():
        if code.strip().lower() == code_cmp.lower():
            return name

    # Long names pass through (possibly being made title case)
    for name in NAMES_TO_CODES:
        if code.strip().lower() == name.lower():
            return name  # The correctly capitalised version

    raise KeyError('Unknown party code {}'.format(code))


def to_short_code(name):
    """Convert a party name into a short code.

    Conversion is case insensitive and will also accept a short code
    which will be returned with the expected capitalisation.

    Parameters
    ----------
    name: str
        Long name of the party.

    Returns
    -------
    code: str
        Short code for the party.

    Raises
    ------
    KeyError:
        Value passes that is not recognised as a party.
    ValueError:
        Non-string value passed.

    """

    if not hasattr(name, 'lower'):
        raise ValueError('Cannot interpret {} as a string'.format(name))

    # Straight case insensitive conversion
    for name_cmp, code in NAMES_TO_CODES.items():
        if name.strip().lower() == name_cmp.lower():
            return code

    # Valid Codes pass through (possibly being made correct case)
    for code in NAMES_TO_CODES:
        if name.strip().lower() == code.lower():
            return code  # The correctly capitalised version

    raise KeyError('Unknown party name {}'.format(name))


def parse_data(text, quiet=False):
    """Parse formatted election data into a dict.

    Election data from the source is semi formatted. Extract the vote
    counts by party and construct a dict with the structure:

        {'Party Name': {
            'winner': 'Winning Party',
            'votes': {
                'Party 1': 1000,
                'Party 2': 3000}
            'vote_percent': {
                'Party 1': 25.0,
                'Party 2': 75.0}
        ...
        }}

    The number of seats won by each party is also totalled up.

    Most parsing errors are not fatal and will be returned along with the
    results.

    Parameters
    ----------
    text: str
        String containing the data with one entry per line.
    quiet: bool
        If False parsing errors will be logged to the package logger.
        Set to True to silence any logging.

    Returns
    -------
    constituencies: dict
        Dictionary containing all the parsed results in full for
        all constituencies. Returned as an OrderedDict sorted
        alphabetically.
    parties: dict
        Dictionary containing the number of claimed seats for
        each party.
    errors: list of str
        A list of any errors encountered in the parsing.

    """

    constituencies = {}
    parties = {party: 0 for party in NAMES_TO_CODES}
    errors = []

    for idx, line in enumerate(text.splitlines()):
        line = line.strip()
        # ignore blanks
        if not line:
            continue
        # Parse from end as some constituency names contain commas.
        # Pair up anything that is a number and a valid party until
        # we encounter values that don't make sense, then the rest is
        # assumed to be the constituency.
        # There are no constituencies with integers in their names,
        # but if that changes this might break.
        backline = [token.strip() for token in line.split(',')[::-1]]
        votes = {}
        vote_percent = {}
        while backline:
            try:
                party = to_long_name(backline[0])
            except (KeyError, IndexError):
                # No longer a valid party or too short
                break
            try:
                vote_count = int(backline[1])
            except (ValueError, IndexError):
                # not an integer value or too short
                break

            votes[party] = vote_count

            backline.pop(0)  # done, remove party
            backline.pop(0)  # done, remove votes

        c_name = ', '.join(backline[::-1])

        total_votes = sum(votes.values())
        max_votes = (0, 'Nobody')

        # Calculate percentages of total
        for party, vote_count in votes.items():
            vote_percent[party] = 100*vote_count/total_votes
            if vote_count > max_votes[0]:
                max_votes = (vote_count, party)

        # Check for any obvious errors
        # Would need to implement a list of all constituencies to
        # have better error catching.
        if c_name.count(',') > 2:
            errors.append('Malformed data on line {}: {}'.format(idx, line))
        elif not votes:
            errors.append('No data found on line {}: {}'.format(idx, line))
        else:
            # Only add if the data is fine
            constituencies[c_name] = {
                'winner': max_votes[1],
                'votes': votes,
                'vote_percent': vote_percent
            }
            # Add a seat to the party
            parties[max_votes[1]] += 1

    # Alphabetise the constituencies
    sorted_constituencies = OrderedDict()
    for c_name in sorted(constituencies):
        sorted_constituencies[c_name] = constituencies[c_name]

    if not quiet and errors:
        for error_msg in errors:
            error(error_msg)

    return sorted_constituencies, parties, errors
