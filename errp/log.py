"""
Logging of important information.

A simple setup with stdlib logging module. To be extended with extra
configuration capability in future.

To use in a module, import the logging functions required, e.g.

    from errp.log import debug
    debug('debugging info')

"""

import logging

# Log to a file and screen for now. In future make configurable so
# that we can log e.g. to remote machines, via email...
logger = logging.getLogger('election_relay')
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

logfile = logging.FileHandler('election_relay_error.log')
logfile.setLevel(logging.INFO)
logfile.setFormatter(formatter)

logscreen = logging.StreamHandler()
logscreen.setLevel(logging.DEBUG)
logscreen.setFormatter(formatter)

logger.addHandler(logfile)
logger.addHandler(logscreen)

# Export the methods as functions that are directly linked
# to the logger
debug = logger.debug
info = logger.info
warning = logger.warning
error = logger.error
critical = logger.critical

# These are the only things that you should be using from
# this module
__all__ = (debug, info, warning, error, critical)
