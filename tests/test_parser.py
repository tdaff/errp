"""
Parser unit tests.

Define the expectations of data in -> data out.

"""

import pytest

from errp.parser import parse_data
from errp.parser import to_long_name, to_short_code


def test_example():
    """Test with provided examples."""
    # An example of the source data was provided with the specification,
    # ensure it is parsed to the expected result

    example_data = """Cardiff West, 11014, C, 17803, L, 4923, UKIP, 2069, LD
    Islington South & Finsbury, 22547, L, 9389, C, 4829, LD, 3375, UKIP, 3371, G, 309, Ind"""

    expected_result = {
        'Cardiff West': {
            'winner': 'Labour Party',
            'votes': {
                'Conservative Party': 11014,
                'Labour Party': 17803,
                'UKIP': 4923,
                'Liberal Democrats': 2069,
            },
            'vote_percent': {
                'Conservative Party': 100*11014/35809,
                'Labour Party': 100*17803/35809,
                'UKIP': 100*4923/35809,
                'Liberal Democrats': 100*2069/35809,
            }
        },
        'Islington South & Finsbury': {
            'winner': 'Labour Party',
            'votes': {
                'Labour Party': 22547,
                'Conservative Party': 9389,
                'Liberal Democrats': 4829,
                'UKIP': 3375,
                'Green Party': 3371,
                'Independent': 309,
            },
            'vote_percent': {
                'Labour Party': 100*22547/43820,
                'Conservative Party': 100*9389/43820,
                'Liberal Democrats': 100*4829/43820,
                'UKIP': 100*3375/43820,
                'Green Party': 100*3371/43820,
                'Independent': 100*309/43820,
            }
        }
    }

    expected_parties = {
        'Conservative Party': 0,
        'Labour Party': 2,
        'UKIP': 0,
        'Liberal Democrats': 0,
        'Green Party': 0,
        'Independent': 0,
        'SNP': 0
    }

    assert parse_data(example_data)[0] == expected_result
    assert parse_data(example_data)[1] == expected_parties


def test_failed_parse():
    """Some known parsing failure test cases."""
    no_data = 'Empty constituency'
    assert 'No data' in parse_data(no_data)[2][0]
    malformed = 'Badly, constructed, data, line'
    assert 'Malformed' in parse_data(malformed)[2][0]


def test_name_convert():
    """Conversion of party names to long and short versions"""
    # To long names
    assert to_long_name('C') == 'Conservative Party'
    assert to_long_name('L') == 'Labour Party'
    assert to_long_name('UKIP') == 'UKIP'
    assert to_long_name('LD') == 'Liberal Democrats'
    assert to_long_name('G') == 'Green Party'
    assert to_long_name('Ind') == 'Independent'
    assert to_long_name('SNP') == 'SNP'
    # And back again
    assert to_short_code('Conservative Party') == 'C'
    assert to_short_code('Labour Party') == 'L'
    assert to_short_code('UKIP') == 'UKIP'
    assert to_short_code('Liberal Democrats') == 'LD'
    assert to_short_code('Green Party') == 'G'
    assert to_short_code('Independent') == 'Ind'
    assert to_short_code('SNP') == 'SNP'
    # errors
    with pytest.raises(KeyError):
        assert to_long_name('XX')
    with pytest.raises(KeyError):
        assert to_short_code('X Party')
    with pytest.raises(ValueError):
        assert to_long_name(1234)
    with pytest.raises(ValueError):
        assert to_short_code(1234)
